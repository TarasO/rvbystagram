class CommentsController < ApplicationController

    skip_before_action :authenticate_request, :only => [:index, :show]

    def index
        @comments = Comment.all
        render json: @comments, status: :ok
      end
    
      # GET /comments/{id}
      def show
        @comment = Comment.find(params[:id])
        render json: @comment, status: :ok
      end
    
      # comment /comments
      def create
        @comment = Comment.new(comment_params)
        @comment.user_id = current_user.id
        if @comment.save
          render json: @comment, status: :created
        else
          render json: { errors: @comment.errors.full_messages },
                 status: :unprocessable_entity
        end
      end
    
      # PUT /comments/{id}
      def update
        unless @comment.update(comment_params)
          render json: { errors: @comment.errors.full_messages },
                 status: :unprocessable_entity
        end
      end
    
      # DELETE /comments/{id}
      def destroy
        @comment.destroy
      end
    
      private
    
      def comment_params
        params.require(:comment).permit(:body, :post_id)
      end

end
