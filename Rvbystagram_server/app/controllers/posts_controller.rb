class PostsController < ApplicationController
  
  skip_before_action :authenticate_request, :only => [:index, :show]

    # GET /posts
  def index
    @posts = Post.all()
    render json: @posts.to_json(:include => {:user => {:only => [:id, :name, :profile_img_url]}, 
      :comments => {:include => {:user => {:only => [:id, :name, :profile_img_url]}}, :except => [:post_id, :user_id, :updated_at]}}, 
      :except => [:updated_at, :user_id]), status: :ok
  end

  # GET /posts/{id}
  def show
    @post = Post.find(params[:id])
    render json: @post, status: :ok
  end

  # POST /posts
  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id 
    if @post.save
      render json: @post, status: :created
    else
      render json: { errors: @post.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  #POST /posts/{id}/photo/
  def photo
    file = params[:photo]
    result = Cloudinary::Uploader.upload(file, :use_filename => true, :folder => "posts")
    post = Post.find(params[:id])
    post.update_attribute(:image_url, result['url'])
    render json: {response: result['url']}, status: :ok
  end

  # PUT /posts/{id}
  def update
    unless @post.update(post_params)
      render json: { errors: @post.errors.full_messages },
             status: :unprocessable_entity
    end
  end  

  # DELETE /posts/{id}
  def destroy
    @post.destroy
  end

  private

  def post_params
    params.require(:post).permit(:body, :image_url, comments: [])
  end
end
