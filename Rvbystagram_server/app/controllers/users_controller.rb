class UsersController < ApplicationController

  skip_before_action :authenticate_request, :only => [:index, :show, :create, :photo]

  # GET /users
  def index
    @users = User.all
    render json: @users, status: :ok
  end

  # GET /users/{id}
  def show
    @user = User.find(params[:id])
    render json: @user, status: :ok
  end

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: :created
    else
      render json: { errors: @user.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  #POST /users/{id}/photo/
  def photo
    file = params[:photo]
    result = Cloudinary::Uploader.upload(file, :use_filename => true, :folder => "users")
    user = User.find(params[:id])
    user.update_attribute(:profile_img_url, result['url'])
    render json: {response: result['url']}, status: :ok
  end

  #GET /users/profile/{name}
  def profile
    @user = User.find_by name: params[:name]
    render json: @user.to_json(:include => {:posts => {:include => {
      :comments => {:include => {:user => {:only => [:id, :name, :profile_img_url]}}, :except => [:post_id, :user_id, :updated_at]}},
      :except => [:user_id, :updated_at]}}, :except => [:created_at, :updated_at, :password_digest]), status: :ok
  end

  # PUT /users/{id}
  def update
    unless @user.update(user_params)
      render json: { errors: @user.errors.full_messages },
             status: :unprocessable_entity
    end
  end

  # DELETE /users/{id}
  def destroy
    @user.destroy
  end

  private

  # def find_user
  #   @user = User.find_by_name!(params[:name])
  #   rescue ActiveRecord::RecordNotFound
  #     render json: { errors: 'User not found' }, status: :not_found
  # end

  def user_params
    params.require(:user).permit(:name, :email, :profile_img_url, :password, :password_confirmation)
  end

end
