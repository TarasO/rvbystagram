Rails.application.routes.draw do
  
  resources :users
  resources :posts
  resources :comments
  post 'authenticate', to: 'authentication#authenticate'
  post 'users/:id/photo', to: 'users#photo'
  post 'posts/:id/photo', to: 'posts#photo'
  get 'users/profile/:name', to: 'users#profile'
end
