let serverUrl = 'http://localhost:3000/';

let token = window.localStorage.getItem('rvbystagram_token');
let user;

if(token != null){
    var user_id = jwt_decode(token).user_id;

    $.ajax({
        url: serverUrl + 'users/' + user_id,
        method: 'GET',
        dataType: 'json',
        async: false,
        contentType: 'application/json',
        complete: function(data) {
            if(data.status == 200) {
                user = data.responseJSON;
            } else if(data.status == 500) {
                console.log('Internal Server Error');
            } 
        }
    });

}