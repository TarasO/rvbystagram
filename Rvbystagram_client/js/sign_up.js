$(document).ready(function() {
    $('#sign-up-form').submit(function(e) {
        e.preventDefault();

        let username = $('#username').val();
        let email = $('#email').val();
        let password = $('#password').val();
        let password_confirmation = $('#password_confirmation').val();

        let user = {
            user: {
                name: username,
                email: email,
                profile_img_url: 'https://res.cloudinary.com/dugzo7z7s/image/upload/v1585602829/users/blank.png',
                password: password,
                password_confirmation: password_confirmation
            }
        };

        $.ajax({
            url: serverUrl + 'users/',
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(user),
            complete: function(data) {
                if(data.status == 201) {
		            if(document.getElementById("photo").files.length != 0){
                    	uploadPhoto(data.responseJSON.id);
                    }
                    $('#sign-up-form')[0].reset();
                    $(location).attr('href', 'sign_in.html');
                } else if(data.status == 500) {
                    console.log('Error has occured');
                }
            }
        });
    });
});

function uploadPhoto(userId) {
    var photo = new FormData();
    photo.append('photo', $('#photo')[0].files[0]);

    $.ajax({
        url: serverUrl + 'users/' + userId + '/photo/',
        method: 'POST',
	    contentType: false,
        data: photo,
        processData: false
    });
}