$(document).ready(function() {
    $('#sign-in-form').submit(function(e) {
        e.preventDefault();

        let email = $('#email').val();
        let password = $('#password').val();

        let user = {
            email: email,
            password: password
        };

        $.ajax({
            url: serverUrl + 'authenticate/',
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(user),
            complete: function(data) {
                if(data.status == 200) {
                    window.localStorage.setItem('rvbystagram_token', data.responseJSON.auth_token);
                    $(location).attr('href', 'index.html');
                } else if(data.status == 401) {
                    console.log('Invalid credentials');
                }
                
            }
        });
    });
});