const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'Authorization' : 'Bearer ' + token
        }
    });

    var profile = getUser();

    $('.post-img').on('click',function(e) {
        let post_index = e.target.parentElement.id;
        $('.modal-img').eq(0).attr('id', profile.posts[post_index].id);
        $('.modal-img').eq(0).attr('src', profile.posts[post_index].image_url);
        $('.post-date')[0].innerText = convertDate(new Date(profile.posts[post_index].created_at));
        $('.post-body')[0].innerText = profile.posts[post_index].body;

        $('.comments-container').eq(0).empty();

        $.each(profile.posts[post_index].comments, function(key, value) {
            $('.comments-container').eq(0).append(
                `
                <div class="row comment">
                    <a class="col-2" href="${(location.hostname+location.pathname).replace("index", "profile") + '?name=' + value.user.name}"><img src="${value.user.profile_img_url}" class="post-prof-pic"> ${value.user.name}</a>
                    <span class="col-7">${value.body}</span>
                    <span class="col-3">${convertDate(new Date(value.created_at))}</span>
                </div>
                `
                );
        });
        $('.comments-container').eq(0).append(
            `
            <div class="post-comment">
                <form class="comment-form form-inline">
                    <span><img src="${user.profile_img_url}" class="post-prof-pic"> ${user.name}</span>
                    <div class="form-group">
                        <input type="text" class="form-control comment-input">
                        <button type="submit" class="btn btn-outline-primary" placeholder="Enter comment">Send</button>
                    </div>
                </form>
            </div>
            `
        );

        $('.comment-form').submit(function(e) {
            e.preventDefault();
            let button = $(document.activeElement);
    
            let body = button.prev().val();
            
            let post_id = $('.modal-img')[0].id;
    
            let comment = {
                comment: {
                    body: body,
                    post_id: post_id
                }
            }
    
            $.ajax({
                url: serverUrl + 'comments/',
                method: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(comment),
                complete: function(data) {
                    if(data.status == 201) {
                        button[0].parentElement.parentElement.reset();
                    } else if(data.status == 500) {
                        console.log('Error has occured');
                    }
                }
            })
        });
    });
});

function getUser() {
    return $.ajax({
        url: serverUrl + 'users/profile/' + urlParams.get('name'),
        method: 'GET',
        async: false,
        dataType: 'json',
        contentType: 'application/json',
        success: function(data) {
            console.log(data);
            $('#profile-img-cont').eq(0).append(
                `
                <img src="${data.profile_img_url}" id="profile-pic">
                `
            );

            $('#name-cont').append(
                `<p id="profile-name">${data.name}</p>`
            );

            $.each(data.posts.reverse(), function(key, value) {
                $('#post-container').append(
                    `
                    <div class="col-3 post" id="${key}" data-toggle="modal" data-target="#open-post-modal">
                        <img class="post-img" src="${value.image_url}"></img>
                    </div>
                    `
                );
            });
        }
    }).responseJSON;
}

function convertDate(date) {
    let str = date.getFullYear() + '.' + (date.getMonth()+1) + '.' + date.getDate() + ' ' + date.getHours() + ':';
    let mins = date.getMinutes();
    if(mins < 10) {
        str += '0' + mins;
    } else {
        str += mins;
    }
    return str;
}