$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'Authorization' : 'Bearer ' + token
        }
    });

    getPosts();

    $('.comment-form').submit(function(e) {
        e.preventDefault();
        let button = $(document.activeElement);

        let body = button.prev().val();
        
        let post_id = button[0].parentElement.parentElement.parentElement.parentElement.id.substr(4);

        let comment = {
            comment: {
                body: body,
                post_id: post_id
            }
        }

        $.ajax({
            url: serverUrl + 'comments/',
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(comment),
            complete: function(data) {
                if(data.status == 201) {
                    button[0].parentElement.parentElement.reset();
                } else if(data.status == 500) {
                    console.log('Error has occured');
                }
            }
        })
    });
});

function getPosts() {
    $.ajax({
        url: serverUrl + 'posts',
        method: 'GET',
        async: false,
        dataType: 'json',
        contentType: 'application/json',
        success: function(data) {
            $.each(data.reverse(), function(key, value) {
                $('#post-container').append(
                    `
                    <div class="post" id="post${value.id}">
                        <div class="post-user">
                            <a href="${(location.hostname+location.pathname).replace("index", "profile") + '?name=' + value.user.name}">
                            <img src="${value.user.profile_img_url}" class="post-prof-pic">${value.user.name}</a>
                            <span class="post-date">${convertDate(new Date(value.created_at))}</span>
                        </div>
                        
                        <div class="img-container">
                            <img src="${value.image_url}" class="post-img">
                        </div>

                        <div class="body-container">
                            <p class="post-body">${value.body}</p>
                        </div>

                        <div class="comments-container">
                        </div>
                        <div class="post-comment">
                            <form class="comment-form form-inline">
                                <span><img src="${user.profile_img_url}" class="post-prof-pic"> ${user.name}</span>
                                <div class="form-group">
                                    <input type="text" class="form-control comment-input">
                                    <button type="submit" class="btn btn-outline-primary" placeholder="Enter comment">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    `
                );
                let post_id = value.id;
                if(value.comments.length > 0){
                    $.each(value.comments, function(key, value){
                        $('#post' + post_id).children('.comments-container').append(
                            `
                            <div class="row comment">
                                <a class="col-2" href="${(location.hostname+location.pathname).replace("index", "profile") + '?name=' + value.user.name}"><img src="${value.user.profile_img_url}" class="post-prof-pic"> ${value.user.name}</a>
                                <span class="col-7">${value.body}</span>
                                <span class="col-3">${convertDate(new Date(value.created_at))}</span>
                            </div>
                            `
                        );
                    });
                }
            });
        }
    });
}

function convertDate(date) {
    let str = date.getFullYear() + '.' + (date.getMonth()+1) + '.' + date.getDate() + ' ' + date.getHours() + ':';
    let mins = date.getMinutes();
    if(mins < 10) {
        str += '0' + mins;
    } else {
        str += mins;
    }
    return str;
}