$('#prof-link')[0].href = (location.hostname+location.pathname).replace("index", "profile") + '?name=' +user.name;
$('#prof-link')[0].innerText = user.name;
$('#prof-link').eq(0).append(
    `
    <img src="${user.profile_img_url}" id="prof-pic">
    `
);

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'Authorization' : 'Bearer ' + token
        }
    });

    $('#search-user').on('keyup', function(e) {
        if(e.keyCode === 13) {
            $(location).attr('href', 'profile.html?' + $('#search-user').val());
        }
    });

    $('#sign-out-button').on('click', function() {
        window.localStorage.removeItem('rvbystagram_token');

        $(location).attr('href', 'sign_in.html');
    });

    $('#add-post-form').submit(function(e) {
        e.preventDefault();

        let body = $('#body').val();

        let post = {
            post: {
                body: body
            }
        };

        $.ajax({
            url: serverUrl + 'posts/',
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(post),
            complete: function(data) {
                if(data.status == 201) {
		            if(document.getElementById("photo").files.length != 0){
                    	uploadPhoto(data.responseJSON.id);
                    }
                    $('#add-post-form')[0].reset();
                } else if(data.status == 500) {
                    console.log('Error has occured');
                }
            }
        });
    });
});

function uploadPhoto(postId) {
    var photo = new FormData();
    photo.append('photo', $('#photo')[0].files[0]);

    $.ajax({
        url: serverUrl + 'posts/' + postId + '/photo/',
        method: 'POST',
	    contentType: false,
        data: photo,
        processData: false
    });
}